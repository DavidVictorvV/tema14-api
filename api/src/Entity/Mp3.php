<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\Mp3Repository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: Mp3Repository::class)]
#[ApiResource]
class Mp3
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'string', length: 255)]
    private $title;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $date;

    #[ORM\Column(type: 'string', length: 255)]
    private $path;

    #[ORM\OneToMany(mappedBy: 'mp3', targetEntity: Marturii::class)]
    private $testimony;

    public function __construct()
    {
        $this->testimony = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return Collection<int, Marturii>
     */
    public function getTestimony(): Collection
    {
        return $this->testimony;
    }

    public function addTestimony(Marturii $testimony): self
    {
        if (!$this->testimony->contains($testimony)) {
            $this->testimony[] = $testimony;
            $testimony->setMp3($this);
        }

        return $this;
    }

    public function removeTestimony(Marturii $testimony): self
    {
        if ($this->testimony->removeElement($testimony)) {
            // set the owning side to null (unless already changed)
            if ($testimony->getMp3() === $this) {
                $testimony->setMp3(null);
            }
        }

        return $this;
    }
}
